# Summary

## Pengantar

* [Ringkasan](/README.md)
* [Pengantar H. Rosihan Anwar](/pengantar/pengantar-h-rosihan-anwar.md)
* [Pengantar Penulis](/pengantar/pengantar-penulis.md)
* [Catatan tentang tulisan ini](pengantar/catatan-tentang-tulisan-ini.md)

## Babak 1: Hidup di Masa Penjajahan

* [Asal-usul dan keadaan keluarga besar saya ](babak-1-hidup-di-masa-penjajahan/asal-usul-dan-keadaan-keluarga-besar-saya.md)
* [Masa kecil dan masa sekolah HIS di Blitar ](babak-1-hidup-di-masa-penjajahan/masa-kecil-dan-masa-sekolah-his-di-blitar.md)
* [Masa remaja di Kediri ](babak-1-hidup-di-masa-penjajahan/masa-remaja-di-kediri.md)
* [Revolusi Agustus dan mulainya pertempuran di Surabaya ](babak-1-hidup-di-masa-penjajahan/revolusi-agustus-dan-mulainya-pertempuran-di-surabaya.md)

## Babak 2: Pengembaraan

* [Mengembara di Jawa dan Sumatera](/babak-2-pengembaraan/mengembara-di-jawa-dan-sumatera.md)
* [Nyawa ekstra di Surabaya ](/babak-2-pengembaraan/nyawa-ekstra-di-surabaya.md)
* [Menjadi guru di Malang ](/babak-2-pengembaraan/menjadi-guru-di-malang.md)
* [Dari Surabaya  ke pertempuran lagi ](/babak-2-pengembaraan/dari-surabaya-ke-pertempuran-lagi.md)
* [Dari pelayan hotel ke penterjemah](/babak-2-pengembaraan/dari-pelayan-hotel-ke-penterjemah.md)

## Babak 3: Menjadi Wartawan

* [Dari korektor menjadi wartawan](babak-3-menjadi-wartawan/dari-korektor-menjadi-wartawan.md)
* [Ke luar negeri untuk pertama kali](babak-3-menjadi-wartawan/ke-luar-negeri-untuk-pertama-kali.md)
* [Bekerja di Harian Rakjat ](babak-3-menjadi-wartawan/bekerja-di-harian-rakjat.md)
* [Dengan Harian PENERANGAN di Padang ](babak-3-menjadi-wartawan/dengan-harian-penerangan-di-padang.md)
* [PRRI dan Operasi 17 Agustus ](babak-3-menjadi-wartawan/prri-dan-operasi-17-agustus.md)

## Babak 4: Wartawan di Gelanggang Internasional

* [Pernikahan dengan gadis Solok ](babak-4-wartawan-di-gelanggang-internasional/pernikahan-dengan-gadis-solok.md)
* [Pekerjaan di suratkabar EKONOMI NASIONAL ](babak-4-wartawan-di-gelanggang-internasional/pekerjaan-di-suratkabar-ekonomi-nasional.md)
* [Konferensi Wartawan Asia-Afrika \(KWAA\) ](babak-4-wartawan-di-gelanggang-internasional/konferensi-wartawan-asia-afrika-kwaa.md)
* [Kegiatan di PWAA dan PWI Pusat ](babak-4-wartawan-di-gelanggang-internasional/kegiatan-di-pwaa-dan-pwi-pusat.md)
* [Kongres I.O.J. di Chili dan G30S](babak-4-wartawan-di-gelanggang-internasional/kongres-ioj-di-chili-dan-g30s.md)
* [Sekretariat PWAA pindah ke Peking ](babak-4-wartawan-di-gelanggang-internasional/sekretariat-pwaa-pindah-ke-peking.md)
* [Perjalanan terakhir sebagai delegasi PWAA ](babak-4-wartawan-di-gelanggang-internasional/perjalanan-terakhir-sebagai-delegasi-pwaa.md)

## Babak 5: Menjadi Orang "Kelayaban"

* [Kehidupan di Tiongkok selama tujuh tahun](babak-5-menjadi-orang-kelayaban/kehidupan-di-tiongkok-selama-tujuh-tahun.md)
* [Persiapan-persiapan meninggalkan Tiongkok ](babak-5-menjadi-orang-kelayaban/persiapan-persiapan-meninggalkan-tiongkok.md)
* [Minta suaka politik di Paris ](babak-5-menjadi-orang-kelayaban/minta-suaka-politik-di-paris.md)
* [Menciptakan pijakan-pijakan pertama di Paris ](babak-5-menjadi-orang-kelayaban/menciptakan-pijakan-pijakan-pertama-di-paris.md)
* [Bekerja di Kementerian Pertanian selama tujuh tahun ](babak-5-menjadi-orang-kelayaban/bekerja-di-kementerian-pertanian-selama-tujuh-tahun.md)
* [Kegiatan-kegiatan di luar pekerjaan di Kementerian Pertanian Perancis ](babak-5-menjadi-orang-kelayaban/kegiatan-kegiatan-di-luar-pekerjaan-di-kementerian-pertanian-perancis.md)
* [Kegiatan-kegiatan mengenai Tapol dan Ex-tapol ](babak-5-menjadi-orang-kelayaban/kegiatan-kegiatan-mengenai-tapol-dan-ex-tapol.md)
* [Fête de l’Humanité ](babak-5-menjadi-orang-kelayaban/fete-de-lhumanite.md)

## Babak 6: Rantai yang Kembali Tersambung

* [Pertemuan kembali yang pertama kali dengan istri ](babak-6-rantai-yang-kembali-tersambung/pertemuan-kembali-yang-pertama-kali-dengan-istri.md)
* [Kedatangan kedua anak di Perancis ](babak-6-rantai-yang-kembali-tersambung/kedatangan-kedua-anak-di-perancis.md)
* [Di depan pengadilan Perancis ](babak-6-rantai-yang-kembali-tersambung/di-depan-pengadilan-perancis.md)
* [Meninggalkan Kementerian Pertanian ](babak-6-rantai-yang-kembali-tersambung/meninggalkan-kementerian-pertanian.md)
* [Persiapan-persiapan berdirinya restoran Indonesia ](babak-6-rantai-yang-kembali-tersambung/persiapan-persiapan-berdirinya-restoran-indonesia.md)
* [Pencarian dana yang berliku-liku ](babak-6-rantai-yang-kembali-tersambung/pencarian-dana-yang-berliku-liku.md)
* [Bekerja di restoran ](babak-6-rantai-yang-kembali-tersambung/bekerja-di-restoran.md)
* [Ciri-ciri utama restoran kita ](babak-6-rantai-yang-kembali-tersambung/ciri-ciri-utama-restoran-kita.md)
* [Keluarga berkumpul kembali ](babak-6-rantai-yang-kembali-tersambung/keluarga-berkumpul-kembali.md)

## Babak 7: Menjadi Pemilik Majalah dengan Membayar 1 F.

* [Pentingnya kegiatan ](babak-7-menjadi-pemilik-majalah-dengan-membayar-1-f/pentingnya-kegiatan.md)
* [Menjadi warganegara Perancis ](babak-7-menjadi-pemilik-majalah-dengan-membayar-1-f/menjadi-warganegara-perancis.md)
* [Mendirikan China Documentation & Communication ](babak-7-menjadi-pemilik-majalah-dengan-membayar-1-f/mendirikan-china-documentation-and-communication.md)
* [Kegiatan sebagai wartawan di Paris ](babak-7-menjadi-pemilik-majalah-dengan-membayar-1-f/kegiatan-sebagai-wartawan-di-paris.md)
* [Mengapa menerbitkan majalah tentang Tiongkok? ](babak-7-menjadi-pemilik-majalah-dengan-membayar-1-f/mengapa-menerbitkan-majalah-tentang-tiongkok.md)
* [Majalah BUSINESS WITH CHINA ](babak-7-menjadi-pemilik-majalah-dengan-membayar-1-f/majalah-business-with-china.md)

## Babak 8: Satu dari Enam Milyar

* [Melihat sebagian dari dunia ](babak-8-satu-dari-enam-milyar/melihat-sebagian-dari-dunia.md)
* [Jalan hidup yang berliku-liku ](babak-8-satu-dari-enam-milyar/jalan-hidup-yang-berliku-liku.md)
* [Pegangan hidup saya ](babak-8-satu-dari-enam-milyar/pegangan-hidup-saya.md)
* [Pandangan saya sekarang mengenai berbagai hal ](babak-8-satu-dari-enam-milyar/pandangan-saya-sekarang-mengenai-berbagai-hal.md)
* [Penutup kata ](babak-8-satu-dari-enam-milyar/penutup-kata.md)

## Epilog

* [Epilog Joesoef Isak](/epilog/epilog-joesoef-isak.md)
* [Riwayat hidup singkat penulis](/epilog/riwayat-hidup-singkat-penulis.md)




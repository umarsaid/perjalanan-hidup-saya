# Pertemuan kembali yang pertama kali dengan istri 

Pertemuan kembali yang untuk pertama kalinya dengan istri adalah dalam tahun 1978. Itu terjadi di Belanda, sesudah kami berpisah tanggal 14 September 1965 di bandar udara Kemayoran untuk pergi ke Chili. Artinya, sesudah tiga belas tahun berpisah, yang disebabkan oleh situasi.

Selama saya ada di RRT, dan kemudian sesudah bermukim di Perancis selama empat tahun, saya tidak ada hubungan dengan keluarga, karena saya tidak tahu di mana alamatnya dan bagaimana cara untuk bisa menghubunginya, tanpa menimbulkan risiko bagi mereka, istri dan anak-anak. Jadi, saya menunggu dengan sabar, sampai saatnya tiba. 

Hubungan kembali ini dimungkinkan oleh kedatangan di Paris, dalam tahun 1977, seorang teman lama, yaitu Joesoef Isak. Dalam pembicaraan kami di kamar sempit di Bastille \(rue de Lappe\), ia menanyakan apakah saya ada hubungan dengan keluarga saya. Saya jawab bahwa tidak tahu di mana mereka tinggal waktu itu, dan bahwa ragu untuk menulis surat ke alamat kami yang lama di Kepu Selatan. Ia berjanji bahwa sekembali di Jakarta ia akan berusaha mencari tahu bagaimana keadaan keluarga saya itu.

Kira-kira setahun kemudian, saya menerima surat dari sahabat lama ini yang menegaskan bahwa setelah bertanya ke mana-mana, maka ditemukanlah alamat istri saya dan bahkan sudah menemuinya. Tidak lama kemudian, pada suatu pagi hari, saya menerima telepon dari istri saya dari Jakarta. Inilah percakapan kami yang pertama kali sejak tiga belas tahun berpisah. 

Tidak perlulah ditulis bagaimana perasaan saya waktu itu. Sejak itu, maka diaturlah cara-cara untuk berkorespondensi. Kemudian, setelah istri saya dapat memperoleh paspor dengan cara-cara yang tidak mudah, maka ia memutuskan untuk datang ke Paris. 

Bersama keluarga Tahsin \(yang tinggal di Belanda\) saya menjemput istri di bandar udara Amsterdam. Dengan sengaja, rute perjalanan memang diatur demikian. Tidak langsung ke Paris. Waktu itu kami masih sangat hati-hati, bahkan mungkin agak keterlaluan. Tetapi, untuk menjaga segala kemungkinan, dan untuk ketenteraman hati istri saya, lebih baiklah begitu. Kami menginap satu malam di keluarga Tahsin, dan keesokan harinya dengan kereta api ke Paris, ke apartemen yang baru \(HLM\) di Soisy Montmorency.

Bermacam-macamlah cerita istri saya tentang pengalaman selama berpisah. Tentang bagaimana dalam hari-hari dan minggu-minggu pertama, menghadapi situasi setelah terjadinya G30S. Semua buku-buku, foto, dan segala barang yang ada hubungan dengan saya telah dibakar. 

Dalam jangka lama sekali, sampai anak-anak menjadi besar, telah dikatakan bahwa bapaknya tidak ada. Ia mulai hidup dengan menjahit pakaian, dan kemudian bekerja di salah satu apotik di Jakarta.

Untuk mencari ketenteraman hati dan untuk keselamatan seluruh keluarga \(bapak ibu, adik-adik dan kedua anak\), mereka kemudian pindah dari rumah yang di Kepu Selatan \(dekat Pasar Senen\). Hubungan dengan kenalan-kenalan lama atau temanteman saya telah diputuskan atau dihindari sama sekali oleh istri saya. Karena, kebanyakan teman-teman telah ditahan atau dibunuh. Suasana yang penuh dengan ketakutan ini selalu menghantui seluruh keluarga, dalam jangka yang lama sekali. Seperti halnya yang dialami oleh banyak sekali keluarga di Indonesia waktu itu, bahkan sampai sekarang, tiga puluh tahun kemudian.

Dengan hidup bersama-sama dengan bapak ibu, kakak dan adik-adiknya, istri saya membesarkan kedua anak \(yang saya tinggalkan ketika masih berumur empat tahun dan satu tahun\), sampai mereka masing-masing memasuki  ITB. Istri saya berusaha dengan segala daya, supaya kedua anak dapat belajar dengan baik. \(Cerita-cerita tentang kehidupan mereka selama itu beraneka-ragam\). 

Karena mendapat cuti dan bantuan sekedarnya dari SMAR, maka kami mengadakan perjalanan ke Italia untuk seminggu lamanya. Kemudian, selama ia tinggal di Paris hampir tiga bulan, kami sering sekali diundang oleh teman-teman Perancis. Banyak yang ingin menyatakan ikut sukacita mereka atas peristiwa kami berdua ini. Sebab, bagi mereka, pertemuan kembali setelah perpisahan tiga belas tahun adalah hal yang luar biasa. 

Dengan kedatangan istri saya di Paris, dapatlah kami bicarakan tentang kehidupan keluarga selanjutnya, antara lain tentang masalah kelanjutan sekolah anak-anak. Sejak itu pulalah ibu saya di Blitar tahu bahwa saya ada di Perancis. \(Waktu itu, bapak sudah wafat di Blitar\).


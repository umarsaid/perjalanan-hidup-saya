# PRRI dan Operasi 17 Agustus 

Setelah PRRI diproklamasikan di Bukittingi, situasi makin menjadi lebih sulit lagi bagi saya. Isi suratkabar makin kami ubah. Umpamanya, ketika RPKAD sudah mulai diterjunkan dengan payung di Pakanbaru dan terjadi pertempuran-pertempuran di berbagai kota, maka kami karangkaranglah berita-berita yang kami terima lewat radio atau lewat Morse. Seorang wartawan kami \(Rd. Syt\) pernah bekerja di PHB \(bagian penghubung tentara\), dan karena itu ia bisa mengikuti siaran-siaran yang dikirimkan lewat radio dengan Morse.

Selama itu hubungan pesawat terbang antara Padang dan Jakarta terputus, demikian juga hubungan laut. Kami membuat berita-berita untuk dimuat suratkabar hanya berdasarkan siaran radio, baik RRI Pusat maupun RRI Padang dan Bukittinggi. Beritaberita mengenai makin dekatnya D-day pendaratan tentara Pusat kami ketahui dari berita-berita yang disiarkan oleh wartawanwartawan luar negeri serta hubungan-hubungan antara kapalkapal yang mengangkut tentara Pusat \(lewat bahasa kode Morse\). 

Saya merasa beruntung waktu itu, karena ketika kapal-kapal perang ALRI sudah menunggu di kejauhan dari pantai laut kota Padang, saya tidak mengalami penangkapan. Mungkin, karena pada waktu itu pasukan-pasukan dan aparat-aparat lainnya yang mendukung PRRI sudah ditarik mundur jauh ke pedalaman. Memang kelihatan ada kepanikan waktu itu, terutama setelah kapal-kapal perang itu menembaki bagian-bagian tertentu kota Padang.

Sebelum terjadinya penembakan-penembakan meriam dari kapal, saya bersama wartawan kami itu \(Ridwan S.\) berharihari tidur di kantor, sambil menunggu-nunggu dengan hati yang tak tenang pendaratan Tentara Pusat. Pada tanggal 17 April 1958, ketika kami bangun tidur dan turun dari loteng persembunyian \(di atas kantor\), kami lihat di jalan di depan kantor pasukanpasukan Tentara Pusat. Saya segera mencari tahu di mana berada pimpinan operasi. Di situlah saya bertemu kembali dengan Mayor Sukendro \(bagian Intel AD\). Ketika ada di Jakarta, saya pernah bertemu beberapa kali dengan dia, bersama-sama dengan wartawan-wartawan suratkabar Jakarta lainnya. 

Pimpinan Operasi 17 Agustus menyarankan kepada saya untuk segera menerbitkan kembali Harian Penerangan, walaupun untuk daerah terbatas, karena situasi di sebagian besar daerah masih belum normal. Pada masa-masa selanjutnya kami bekerja sama dengan Bagian Penerangan Operasi 17 Agustus \(Kapten Moein\). Pada tanggal 4 Mei 1958 “ibukota” PRRI, Bukittinggi, jatuh ke dalam tangan Tentara Pusat. 

Walaupun perlawanan PRRI tidaklah begitu kuat waktu itu, tetapi operasi gabungan Angkatan Darat, Angkatan Laut dan Udara \(Komando Operasi 17 Agustus dengan dipimpin oleh Kolonel A. Yani\) ini memakan waktu yang agak lama. Sebab, pasukan-pasukan PRRI dan pemimpin-pemimpin mereka telah ditarik mundur ke daerah-daerah pedalaman yang jauh, untuk melakukan gerilya. 

Setelah tertangkapnya pimpinan PRRI beberapa bulan berikutnya \(antara lain Mr Sjafrudin Prawiranegara, Moh. Natsir, Letkol Ahmad Husein dan lain-lain\), maka kehidupan di Sumatera Barat berangsur-angsur kembali normal. Hubungan lalu lintas yang terputus menjadi lebih lancar, dan sekolah-sekolah mulai di buka kembali. 

Dengan selesainya pemberontakan PRRI, maka berakhirlah ketegangan-ketegangan antara Pusat dan daerah-daerah, yang dikobarkan oleh gerakan-gerakan separatis. Tetapi Pemerintah Pusat waktu itu masih menghadapi banyak persoalan-persoalan sulit lainnya.

Keadaan ekonomi Indonesia makin memburuk. Operasi untuk menumpas pemberontakan daerah-daerah telah memakan biaya yang amat besar. Dengan kekalahan PRRI, sikap permusuhan negara-negara Barat terhadap Presiden Soekarno kelihatan makin keras. 

Setelah PRRI dapat dikalahkan, tenaga redaksi Harian Penerangan diperbanyak dengan merekrut sejumlah teman. Di antara mereka terdapat seorang yang lolos dari kamp konsentrasi “Situjuh,” ketika terjadi pembunuhan besar-besaran. Semua tahanan di kamp ini dibrondong dengan senapan mesin oleh pasukan PRRI, tetapi ia \(Zulkifli Suleman\) sempat menjatuhkan diri dan ditimpa oleh mayat yang lain. Kemudian, untuk menceritakan kejadian-kejadian yang mengerikan ini ia telah membuat buku yang berjudul Laporan dari kamp maut.

Dengan makin terkonsolidasinya situasi normal di Sumatera Barat, maka kegiatan di Sumatera Barat tidak hanya terbatas dalam kewartawanan. Antara lain, saya ikut serta aktif dalam Musyawarah Besar Kebudayaan Adat Sumatera Barat. Pernah juga diajak oleh Peperda \(Penguasa Perang Daerah\) untuk mengunjungi kepulauan Mentawai dan daerah-daerah lain.

Sekarang, ketika menulis Perjalanan Hidup Saya ini saya masih ingat akan saat-saat tertentu semasa bekerja di daerah PRRI ini. Sering sekali merasa takut kalau ditangkap. Sebab, waktu itu saya juga ada hubungan dengan gerakan-gerakan di bawah tanah yang pro-Pusat.

Untuk pekerjaan itu, pernah saya lakukan pertemuanpertemuan malam hari di salah satu kuburan di pinggiran kota Padang. Dengan sejumlah “penghubung” gerakan di bawah tanah telah juga saya atur tanda-tanda tertentu. Umpamanya, kapankah dan bagaimanakah mereka bisa datang ke kantor redaksi suratkabar supaya tidak diketahui oleh orang lain. Atau “jangan masuk” ke kantor, kalau ada tanda-tanda tertentu, sebab waktu itu sedang ada “orang lain” yang tidak perlu mengetahui tentang pertemuan kami. 

Singkatnya, pengalaman selama memimpin koran kecil di tengah-tengah daerah pemberontakan PRRI ini merupakan bagian yang meninggalkan kesan yang sulit dilupakan. Sebab, masa-masa itulah Republik Indonesia mengalami saat-saat yang paling gawat. Pemberontakan PRRI-Permesta adalah, pada intinya, komplotan besar-besaran untuk melawan politik Bung Karno, dalam situasi “Perang Dingin” yang makin memanas waktu itu. Komplotan ini \(yang terdiri dari sejumlah pimpinan militer di daerah-daerah dan tokoh-tokoh partai Masjumi dan PSI\) mendapat dukungan besar dari pihak Barat \(baca: Amerika Serikat\).


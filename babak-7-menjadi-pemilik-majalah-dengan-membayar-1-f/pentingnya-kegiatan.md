# Pentingnya kegiatan 

Sejak minta suaka politik di Perancis dalam bulan September 1974, saya merasa yakin akan pentingnya menggalang persahabatan dengan berbagai orang. Sebab, saya hidup di perantauan, dan waktu itu sedang menghadapi persoalan-persoalan. Terutama masalah permintaan suakapolitik. Ketika itu, saya kuatir sekali bahwa permintaan ini ditolak oleh pemerintah Perancis. Sebab kalau ditolak, maka akan gagallah banyak rencana. Padahal, keberhasilan suaka di Paris ini sangat penting bagi saya dan bagi sejumlah teman-teman Indonesia lainnya. Di samping itu saya ingin menjalankan kegiatan. 

Untuk ini semua, saya berusaha keras untuk mencari kontakkontak yang luas, mencari teman dan menggalang persahabatan, dengan berbagai jalan. Kontak-kontak ini, dalam masa-masa permulaan, dilakukan dengan menghadiri atau ikut serta dalam kegiatan yang diadakan oleh berbagai organisasi atau perkumpulan Perancis. Saya masih ingat bagaimana, pada akhir tahun 1974, sering mengikuti acara-acara weekend yang diadakan oleh Communauté de Base dari golongan kiri Katolik, bersama-sama Odile Chartier, Denis Priyen, Yves Barou \(dari PSU, Parti Sosialiste Unifié\) dan lain-lain.

Dalam kegiatan yang diselenggarakan di suatu biara Katolik itu, didiskusikan berbagai soal yang menyangkut masyarakat Perancis waktu itu: sistim pemerasan oleh kapitalisme, soal Dunia Ketiga, soal demokrasi dan lain-lain. 

Menurut pengalaman, mengikuti kegiatan-kegiatan tertentu, bisa merupakan jalan pintas untuk menjalin kontak yang lebih dekat dengan berbagai orang. Karena, dengan melakukan kegiatan bersama mengenai sesuatu, kita bisa bergaul dengan orang-orang yang sedikitbanyaknya memiliki titik-titik persamaan: pendirian politik, pandangan, kesukaan atau kecenderungan atau kebalikannya. 

Dengan begitu, kita mengenal orang dan mengenal situasi dari kontak langsung. Melalui praktek begini, kita bisa memperluas jaringan perkenalan dan juga persahabatan. 

Permulaan hubungan saya dengan Louis Joinet adalah karena kegiatan-kegiatan mengenai Timor Timur. Ia telah memberikan bantuan untuk menyelesaikan persoalan-persoalan yang dihadapi restoran kita. 

Biasanya, untuk meningkatkan kontak atau hubungan menjadi persahabatan, dibutuhkan syarat-syarat. Orang bisa menjadi sahabat kalau samasama senang dan saling menghargai. Di sini berlaku tuntutan timbal balik. Kita bersahabat dengan orang lain bukan hanya dengan tujuan untuk minta tolong saja, tetapi juga harus bersedia untuk memberikan pertolongan kepadanya.

Orang lain ingin bersahabat dengan kita, karena berbagai sebab juga. Mungkin karena senang dengan kita, atau memerlukan kita. Keperluan ini bisa macam-macam.

Tetapi, kalau kita lihat kemudian bahwa ia hanya mau menarik keuntungan saja dari kita, biasanya persahabatan yang demikian ini tidak langgeng. Persahabatan juga menuntut adanya saling memberi, dalam macam-macam bentuk dan melalui berbagai cara. 

Persahabatan yang bisa dijalin lewat kegiatan bersama \(bentuknya dan bidangnya bisa macam-macam\) biasanya secara relatif bisa awet. Sebab, melalui praktek bersama, kita saling mengenal. Tindakan atau perbuatan konkrit, bukan hanya omongan, bisa merupakan ukuran bagi kita masing-masing. 

Dalam berbagai kegiatan, kita bisa saja melakukan kesalahan, besar atau kecil. Ini lumrah. Sebab, hanya orang yang tidak berbuatlah yang tidak melakukan kesalahan. Yang penting ialah bahwa kita berusaha memperbaiki kesalahan itu dan berusaha meneruskan usaha.

Ketika kita masih kecil, kita semua pun jatuh bangun untuk belajar berjalan. Kalau kita ingat ini semua, kita jadinya juga mudah untuk memaafkan kesalahan atau kekurangan orang lain, atau untuk membantu orang lain memperbaiki kesalahannya.

Dalam kegiatan bersama, melalui praktek untuk menangani macam-macam soal, kelihatanlah kwalitet masing-masing, dari segi kemampuan, kemauan dan pandangan hidup. Dalam perjalanan hidup saya, saya telah temui berbagai peristiwa atau pengalaman, yang menunjukkan bahwa praktek adalah penting, dan melalui prakteklah kita belajar terus menerus. Dalam segala hal.


# Asal-usul dan keadaan keluarga besar saya

Saya dilahirkan pada tanggal 26 Oktober 1928 di Pakis, di suatu desa di dekat kota kecil di Jawa Timur yang bernama Tumpang, beberapa puluh kilometer dari Malang. Kota kecil ini terletak di daerah pegunungan, dan udara sehari-hari di sini agak sejuk. 

Bapak saya lahir di Blitar dan diberi nama Amirun oleh kakek. Kakek adalah orang Madura, yang ketika mudanya merantau di kota Blitar. Banyak saudara-saudara kakek \(dekat dan jauh\) tinggal di Kampung Maduran di kota ini. Setelah menjadi guru dan kawin dengan ibu, bapak mengambil nama Hardjowinoto.

Dari asal kekeluargaan ini, dapatlah dikatakan bahwa saya adalah orang Jawa Timur. Barangkali juga karena itulah maka saya, sampai sekarang, suka sekali makan tempe, pecel dan tahu. Malang terkenal dengan tempenya, Blitar dengan pecelnya dan Kediri dengan tahunya. Mungkin juga, ini disebabkan karena sejak kecil saya tidak suka makan daging. 

Dari pihak bapak, keluarga jauh saya terdiri dari orang-orang Madura yang tinggal di Blitar dan sekitarnya, yang kebanyakan \(waktu di jaman kolonial Belanda\) bekerja sebagai pedagang. Jarang yang menjadi pegawai negeri. Umumnya, mereka adalah orang-orang yang taat menjalani ajaran agama Islam. Bapak saya adalah salah seorang di antara mereka ini yang dianggap menonjol, karena berhasil menamatkan pelajarannya di Normaal School Blitar \(sekolah guru\). 

Saya masih ingat, bagaimana ketika masih kanak-kanak \(sekolah di HIS \) sering dibawa oleh bapak untuk mengunjungi sanak kerabat Madura di Blitar. Kelihatan waktu itu bahwa bapak dihormati oleh orang-orang Kampung Maduran itu. Mungkin, karena pengalaman beliau yang demikian itu pulalah maka bapak sering mengatakan kepada saya, bahwa keinginan bapak ibu ialah supaya saya menjadi “orang.” Kalau saya renungkan kembali masa-masa yang sudah saya lewati, saya merasa bahwa, banyak sedikitnya, pesan bapak itu rupanya mempunyai pengaruh dalam kehidupan saya. Di samping adanya berbagai faktor lainnya.

Berlainan dengan keluarga pihak bapak, keluarga pihak ibu termasuk orang-orang yang terpandang di kota kecil Tumpang. Kakek adalah pengulu di mesjid kota ini, dan sanak-saudaranya juga banyak yang menjadi “piyayi.” Di antara mereka ada yang memakai gelar Panji \(sebutan di Jawa Timur untuk mereka yang setengah bangsawan, waktu itu\). 

Menurut ingatan saya yang remang-remang, ketika masih kecil saya merasakan juga perbedaan ini. Kalau saya dibawa berkunjung ke keluarga di pihak ibu, saya mempunyai perasaan bahwa mereka itu adalah orang-orang yang berpangkat, yang relatif “berada” dan terpandang. Ini kelihatan dari rumah-rumah mereka yang cukup baik, dan tata-cara yang mereka pakai.

Ketika catatan ini mulai ditulis di Paris pada akhir Mei 1995, saya tidak tahu bagaimana keadaan adik-adik saya dan keadaan keluarga jauh atau dekat, baik dari pihak bapak maupun dari pihak ibu. Karena itu, ketika setelah saya mengetahui bahwa ada kemungkinan untuk menghubungi lewat telpon sebagian dari adik-adik saya, saya segera melakukannya. Ini juga dalam rangka penulisan catatan ini. 

Pada tanggal 15 Juni 1995, saya dan istri saya menilpun ke Malang, untuk menghubungi adik saya yang nomor 4. Saya sudah tidak bertemu dengannya selama tiga puluh satu tahun. Tentu saja, ini merupakan peristiwa penting bagi saya, yang menimbulkan macam-macam perasaan dan keharuan. Karena, lewat percakapan telpon yang cukup panjang inilah saya dapat mengetahui, walaupun secara pokok-pokok, berbagai soal yang menyangkut keluarga saya.

Dua hari kemudian saya menilpun adik saya yang nomor 3, yang kebetulan ada di Surabaya waktu itu. Percakapan yang mengharukan lewat telpon dengan dia menambah pengetahuan saya tentang keadaan adik-adik saya dan keluarga lainnya. 

Dari hubungan dengan mereka inilah saya mengetahui bahwa adik-adik saya, dan bahkan anggota-anggota keluarga lainnya, masih dihantui oleh trauma dari kejadian-kejadian sekitar tahun 1965. Saya mendapat kesan bahwa walaupun mereka gembira dengan hubungan yang kami jalin kembali dan walaupun mereka juga menunjukkan kesayangan kepada saya sebagai kakak mereka, terasa bagi saya bahwa mereka masih tercengkam oleh ketakutan.

Rupanya, berita-berita dalam pers Indonesia mengenai kejadian-kejadian di Kuba dalam permulaan tahun 1966 \(Konferensi Tricontinental di Havana\), peristiwa di Siria dalam tahun 1967, artikel-artikel mengenai restoran Indonesia di Paris, atau kegiatan-kegiatan saya di PWI Pusat atau KWAA dan KIAPMA di Indonesia sebelum G30S, mempunyai efek yang sangat serius bagi mereka. Saya dapat mengerti sepenuhnya keadaan atau sikap yang begitu itu. Ini wajar. Sebab, di antara keluarga adik-adik ini ada juga yang mengalami peristiwa yang menyedihkan sekali. 

Pada tanggal 24 Juni 1995, saya mendapat kesempatan untuk berbicara lewat telpon dengan adik-adik saya lainnya, yaitu Sht dan Snyt, yang kebetulan sedang berkumpul di Malang dalam rangka réuni dari keluarga-besar pihak ibu, yang diselenggarakan tanggal 25 Juni 1995 di Malang. Dari pembicaraan ini, saya lebih yakin lagi bahwa trauma berat yang sudah mereka tanggung selama 30 tahun itu masih melekat pada mereka itu semua.

Kenyataan bahwa mereka takut menyebutkan nama Ayik dalam pembicaraan lewat telpon adalah ukuran tentang bagaimana seriusnya trauma ini. Bahkan, adik-adik saya itu, rupanya masih ketakutan jika menerima telpon dari saya, walaupun mereka senang.

Baru kemudianlah saya ketahui, bahwa dalam jangka puluhan tahun, nama saya sudah tidak disebut-sebut lagi sama sekali dalam daftar keluarga adik-adik saya. Artinya, mereka tidak pernah menyatakan \(dalam kertas-kertas resmi atau silsilah dan lain-lain\) bahwa saya pernah ada, sebagai saudara. 

Dari kenyataan semacam itulah saya melihat betapa hebatnya dampak teror sistematis \(dan terus menerus\) yang dilakukan selama puluhan tahun oleh Orde Baru. Entah berapa puluh juta orang di Indonesia yang selama itu tidak berani, atau segan, untuk berhubungan dengan orang-orang yang dikenal sebagai orang “kiri,” komunis, atau pendukung Presiden Soekarno.


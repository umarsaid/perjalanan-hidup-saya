# Melihat sebagian dari dunia

Kegiatan-kegiatan sebagai wartawan di Indonesia di masa lampau dan sebagai pengurus PWI Pusat \(sampai September 1965\) telah memberikan kesempatan kepada saya untuk mengalami atau menyaksikan berbagai peristiwa penting di Indonesia . Setelah bertugas di PWAA, dan melakukan kegiatan dalam rangka IOJ \(International Organisation of Journalists\) dan organisasi-organisasi lainnya, maka pengalaman-pengalaman di bidang kegiatan internasional makin bertambah.

Selama masa-masa yang lalu, untuk berbagai kegiatan yang beraneka-rupa, saya telah mengunjungi sejumlah negeri. Sebagian untuk menghadiri konferensi-konferensi internasional, sebagian lagi karena diundang oleh negeri-negeri tertentu sebagai wartawan, sebagian lagi untuk urusan yang macam-macam. Dari kegiatan-kegiatan inilah saya mengenal berbagai organisasi, tokoh, dan juga berbagai persoalan-persoalan waktu itu. Sebagian besar di antaranya sudah tidak ada lagi \(orang-orangnya, atau organisasinya\) dan banyak persoalan-persoalannya juga tinggal menjadi sejarah saja. Namun, banyak dari pengalamanpengalaman itu menjadi pelajaran atau khasanah hidup yang berharga.

Berbagai negeri yang telah dikunjungi dan peristiwaperistiwa yang dialami, antara lain dan secara pokok-pokok, adalah:

* **Austria:** dalam tahun 1953 untuk Konferensi Hak-Hak Pemuda di Wina 
* Rom**ania:** dalam tahun 1953, untuk mempersiapkan Festival Pemuda Sedunia di Bukares 
* **Tiongkok:** dalam tahun 1953 \(menjadi tamu Gabungan Pemuda Seluruh Tiongkok\), dan dalam tahun 1962 untuk mempersiapkan KWAA, dan kemudian bermukim antara 1965-1973 \(7 tahun\) 
* **Hongkong:** pertama kali dalam tahun 1953, dan sesudah itu berkali-kali sampai sekarang. 
* **Cekoslowakia:** pertama kali untuk mengunjungi Brno International Fair dalam tahun 1961, kemudian dalam 1962 untuk hubungan dengan IOJ, dan sesudah itu sering melewati Praha dalam rangka berbagai kegiatan 
* **Polandia:** undangan Poznan Fair dalam tahun 1962 
* **RDD \(Jerman Timur\):** undangan Leipzig Fair dalam tahun 1963, dan Kongres IOJ di Berlin Timur dalam tahun 1966 \(tetapi ditolak oleh kongres, karena sudah aktif di PWAA di Peking\) 
* **Hongaria: **untuk menghadiri kongres IOJ di Budapest dalam tahun 1962, 
* **Inggris: **dalam tahun 1962, undangan dari British Foreign Office. Interview dengan BBC, siaran Indonesia 
* **Belgia:** dalam tahun 1962, undangan Kementerian Luar Negeri Belgia. Mengunjungi pabrik film Kodak
* **Bulgaria: **undangan International Fair di Plovdiv dalam tahun 1963 
* **Jepang:** menghadiri konferensi Hiroshima dalam tahun 1963, dan mengikuti rombongan Presiden Soekarno ke Manila Pnompenh-Tokio dalam tahun 1962 
* **Kamboja: **singgah dalam tahun 1963 dalam rangka konferensiinternasional di Hanoi untuk menyokong perjuangan Vietnam, dan dalam tahun 1964 mengikuti kunjungan kenegaraan Presiden Soekarno 
* **Vietnam:** konferensi internasional di Hanoi dalam tahun 1963, bersama empat teman Indonesia, berpotret bersama Ho Chi Minh 
* **Mesir:** singgah berkali-kali \(empat hari sampai seminggu\), dalam rangka perjalanan ke negeri-negeri Arab dan Afrika untuk PWAA, dalam tahun-tahun 1962, 1963,1964, 1965, 1967
* **Sudan: **delegasi PWAA bersama Yang Yi dan Francisca Nasution dalam tahun 1963 
* **Ghana:** delegasi OISRAA \(Organisasi Indonesia untuk Setiakawan Rakyat Asia-Afrika\) untuk konferensi di Winneba tahun 1963. Para peserta konferensi diterima oleh Presiden Kwame Nkrumah 
* **Uganda:** sebagai delegasi PWAA tahun 1963, bertemu dengan Ny Soepeni di Kampala \(ibukota Uganda\), yang waktu itu bertugas sebagai Duta Besar Keliling 
* **Tanzania:** sebagai delegasi PWAA tahun 1963, disambut oleh Tu Peiling dari kantor-berita Xinhua di Dar Es Salam 
* **Zanzibar: **sebagai delegasi PWAA tahun 1963, diterima oleh Moh. Salim \(orang Zanzibar, yang kemudian menjadi sekretaris PWAA di Peking selama RBKP\) 
* **Somalia: **sebagai delegasi PWAA tahun 1963 
* **Yemen Selatan:** sebagai delegasi PWAA bersama Abukos \(sekretaris PWAA dari Siria\) dan penterjemah bahasa Arab Adnan Basalamah, dalam tahun 1964 
* **Irak:** idem, dalam tahun 1964 
* **Aljazair:** berkali-kali, konferensi OSRAA \(AAPSO\) di Alger dalam tahun 1963, tahun 1964 untuk menghadiri konferensi IOJ, dan September 1965 untuk mempersiapkan konferensi KWAA ke-2 di Alger ketika terjadi G30S 
* **Chili: **Congres IOJ di Santiago, dalam September 1965, bersama Francisca Fangidai dan seorang teman \(Rahim\) dari Suluh Indonesia
* **Kuba: **delegasi OISRAA \(6 orang, dengan dipimpin oleh Ibrahim Isa\) yang berangkat dari Peking, untuk menghadiri konferensi Trikontinental di Havana. Pembicaraan dengan Fidel Castro di Hotel Havana Libre 
* **Siria: **bersama Aboukos dalam tahun 1964, kemudian tahun 1967 ketika terjadi “peristiwa stempel” di Damascus 
* **Mali: **dengan delegasi PWAA yang berangkat dari Peking bersama Lionel Morrison, tahun 1967. Bertemu dengan Menteri Penerangan Mali Mamadou Gologo 
* **Guinea:** idem, tahun 1967. Bertemu dengan sekretariat Konferensi Jurist Asia-Afrika di Conakry, di mana bekerja Sdr Wiyanto sebagai wakil Indonesia 
* **Congo Brazzaville**: idem, delegasi PWAA, tahun 1967. 
* **Sierra Leone: **idem, delegasi PWAA, tahun 1967. 
* **Maroko: **idem, delegasi PWAA, tahun 1967 
* **Senegal: **idem, delegasi PWAA, tahun 1967. 
* **Perancis: **sering sekali. Pertama kali dalam tahun 1961 \(sekembali dari Brno Fair\). Dalam tahun 1963, selama transit beberapa hari, menghubungi Nguyen Ki, yang menerbitkan majalah Revolution. Majalah ini bekerjasama dengan PWAA. Kemudian, hampir setiap tahun saya sering transit di Paris untuk beberapa hari, dalam rangka berbagai kegiatan internasional. Dalam tahun 1965, pulang ke Jakarta dengan pesawat dengan rombongan Bung Karno dari Paris.
* **Yugoslavia:** dalam tahun 1974, selama satu setengah bulan. Ini persiapan untuk ke Jerman Barat dan kemudian ke Perancis. Di sini berpindah-pindah, Beograd, Zagreb, Lyubiana, Sarayewo, Split dll. Ketika sudah bermukim di Perancis, maka kegiatan saya sudah tidak seperti selama ketika masih bertugas di PWAA. Ini sesuai juga dengan situasi kehidupan yang baru di negeri ini. Namun begitu, masih melakukan perjalanan ke berbagai negeri untuk urusan yang macammacam, antara lain ke: 
* **Jerman Barat:** berkali-kali dalam tahun 1975, sampai 1980, untuk membantu kedatangan sejumlah teman yang perlu bermukim di negeri ini. 
* **Holland: **juga berkali-kali dalam tahun-tahun antara 1976 sampai 1982, untuk berbagai urusan \(antara lain: kontak dengan Prof. Wertheim di Wageningen, urusan-urusan dengan Komite Indonesia, Novib, dan bertemu dengan teman-teman Indonesia\). 
* **Albania: **dalam tahun 1977, mempersiapkan langkah-langkah bagi sejumlah teman Indonesia yang ingin meninggalkan Albania. 
* **Portugal: **dalam tahun 1981, untuk konferensi internasional mengenai Timor Timur. 
* **Mozambique:** dalam tahun 1983, untuk mencari informasi tentang kemungkinan bagi teman-teman Indonesia untuk bermukim di negeri ini. 
* **Korea Utara:** dalam tahun 1986, bersama Sergio Regazzoni dari CCFD \(Di situ bertemu dengan Gatut, anaknya pak Asmu\), dalam rangka projek CCFD dengan Universitas Pertanian Korea Utara. Dalam perjalanan kembali ke Paris, kami singgah di Peking beberapa hari. Untuk pertama kalinya, CCFD saya hubungkan dengan CAFIU \(Chinese Association for International Understanding\), 

Karena menangani penerbitan majalah bulanan Chine Express, yang sejak 1986 memerlukan banyak tenaga dan pikiran \(karena bekerja sendirian, dan dalam keadaan yang cukup sulit\), maka kegiatan-kegiatan lainnya terpaksa makin terbatasi. Perjalanan internasional yang sering dilakukan sejak itu adalah ke Tiongkok. Sebab, sebagai pengelola majalah, saya perlu sebanyak mungkin bisa mengikuti perkembangan situasi politik, ekonomi, sosial negeri ini waktu itu.

Untuk mengadakan perjalanan ke berbagai negeri sejak tahun 1953 itu, saya sudah menggunakan tiga paspor umum dan satu paspor service yang dikeluarkan oleh pemerintah Indonesia, satu travel document yang dikeluarkan oleh pemerintah Tiongkok dan satu travel document lainnya lagi yang dikeluarkan oleh pemerintah Perancis dalam status sebagai political refugee \(peminta suaka politik\). Sayang sekali, bahwa paspor service \(dikeluarkan oleh Deplu Jakarta dalam tahun 1965\) hilang di Roma dalam tahun 1973.


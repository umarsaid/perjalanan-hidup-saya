# Menciptakan pijakan-pijakan pertama di Paris 

Sejak hari kedua tiba di Paris, saya sudah bertemu \(di kantor Comite Vietnam itu\), dengan Odile Chartier dan Denis Priyen, yang waktu itu juga membantu kegiatankegiatan Comite ini. 

Hubungan hari-hari pertama ini kemudian dilanjutkan dengan kegiatan-kegiatan bersama yang bentuknya macammacam. Persahabatan dengan Odile Chartier ini masih berlangsung sampai sekarang. Menggalang persahabatan adalah penting untuk bisa mengadakan langkah-langkah, baik untuk kepentingan pribadi atau untuk kepentingan orang lainnya. 

Apalagi, karena hidup di negeri asing, maka kebutuhan ini lebih terasa lagi. Karena itu, sejak permulaan tiba di Perancis, masalah ini menjadi usaha utama. Dengan melalui kenalankenalan pertama, saya usahakan untuk menerobos lingkunganlingkungan lainnya, yang kemudian makin lama dan melebar. 

Di Perancis, seperti halnya di berbagai negeri lainnya, adalah tidak mudah untuk bisa diterima begitu saja oleh suatu lingkungan. Tetapi, kalau sudah diterima oleh satu lingkungan, dengan sikap yang correct dapatlah kemudian ditembus lingkungan-lingkungan yang lain. Untuk mencapai tujuan ini, sering dituntut kesediaan untuk membantu atau bersikap ringan tangan.

Dalam rangka inilah saya pernah ikut mengecat dan mengerjakan halhal yang lain, ketika toko buku/penerbit l’Harmattan akan dibuka, dan juga sesudahnya. Dan itu pun tanpa bayar. 

Toko buku atau penerbit adalah tempat strategis untuk: mencari kontak-kontak, mengenal berbagai organisasi, berkenalan dengan orang-orang terkemuka di macam-macam lingkungan. Kebetulan sekali, pengurus toko buku/penerbit l’Harmattan ini \(yang tadinya terletak di jalan Rue des Quatre Vents, dekat Odeon\) adalah para bekas pendeta Katolik yang “kiri” atau mempunyai simpati kepada Dunia Ketiga, terutama Afrika. 

Untuk memulai kegiatan politik mengenai Indonesia, saya angkat masalah yang “acceptable”\(bisa diterima\) bagi golongan yang luas, yaitu masalah Tapol \[Tahanan Politik\]. Karena, masalah ini menyangkut kepedulian \(sensibilité\) banyak orang, maka ketika berbagai organisasi Perancis dihubungi, mudahlah kontak selanjutnya dijalin.

Dua setengah bulan sesudah saya tiba di Paris, saya mendapat surat keterangan dari pimpinan majalah Témoignage Chrétien bahwa saya adalah pembantu “freelance” mereka. Ini penting bagi saya yang sedang minta suaka politik waktu itu. 

Tetapi, untuk bisa berkomunikasi dengan baik, perlulah penguasaan bahasa yang memadai. Karena ada di Perancis, maka saya usahakan dengan berbagai cara, dan dengan ketekunan, untuk belajar bahasa ini. Dengan bahasa yang cukup baik, kita lebih mudah untuk berkomunikasi. Ini penting, untuk dapat menyampaikan, menyajikan soal-soal supaya bisa diterima atau dimengerti dengan baik. Maka belajarlah saya lebih lanjut bahasa Perancis lewat satu kursus di Sorbonne, selama beberapa bulan. Ini sambil menunggu keluarnya Kartu Réfugié \(kartu peminta suaka politik\). 

Sebelumnya, saya memang sudah bisa juga bahasa Perancis, sekadar untuk berkomunikasi secara sederhana. Setelah menunggu kira-kira lima bulan, maka saya terimalah Kartu Réfugié dari OFPRA \(Kantor Perancis yang mengurusi orang-orang asing yang minta suaka politik\). Sesudah keluarnya kartu OFPRA ini, maka hati menjadi lega sekali. Sebab, ini berarti bahwa saya tidak bisa diusir lagi dari Perancis, dan juga bahwa saya mendapat perlindungan \(keselamatan\) dari pemerintah Perancis. Ini berarti bahwa pemerintah Indonesia sudah tidak bisa secara sembarangan mengganggu lagi keselamatan saya. Di samping itu, sejak itu saya dapat minta Titre de Voyage Perancis, yang berlaku sebagai paspor untuk mengadakan perjalanan ke berbagai negeri. Maka, sejak itu saya mulai bisa bergerak ke mana saja, tanpa ada ketakutan, karena mendapat perlindungan negara Perancis.

Setelah mendapat Kartu Réfugié ini masih tinggal lagi masalah penting yang harus dipecahkan, yaitu mencari pekerjaan. Sebagai orang asing, yang belum menguasai secara baik bahasa Perancis, sejak semula saya tidak punya ilusi untuk dapat mencari pekerjaan yang muluk-muluk. Jelaslah bahwa walaupun profesi saya selama ini adalah dalam jurnalistik, tetapi untuk bekerja di bidang pers Perancis adalah sulit sekali, walaupun hanya sebagai korektor, atau sebagai tukang ketik. Apalagi bekerja sebagai pegawai negeri di kantor pemerintahan. 

Lama sekali saya mencari, dari iklan-iklan di suratkabar atau majalah-majalah, pekerjaan sebagai jaga malam, upas kantor, atau pekerjaan-pekerjaan lainnya yang tidak memerlukan penguasaan bahasa Perancis secara baik. 

Juga mendaftarkan diri ke suatu “kantor penempatan  tenaga” bagi orang-orang asing yang terletak di depan stasiun Gare de Lyon. Saya juga mendatangi banyak hotel di Paris untuk menawarkan diri sebagai “veilleur de nuit” \(jaga malam di resepsi\) dengan senjata bahwa bisa bahasa Inggris secara baik, sedikit bahasa Jerman dan bahasa Belanda. Pikiran saya waktu itu  ialah bahwa dengan menjadi jaga malam ini, maka saya akan mempunyai banyak waktu untuk belajar bahasa dan banyak membaca.

Pada suatu hari, selama satu malam saya pernah ditest untuk bekerja di hotel Sevres-Babylon, sebagai veilleur de nuit merangkap standardiste \(tukang telepon\). Test ini berjalan tidak lancar, sebab sering melakukan kesalahan ketika harus menyambung telepon, yang waktu itu masih harus dicolok-colokkan. Karena test ini dianggap gagal, maka saya hanya bekerja satu malam saja, dan keesokan harinya dibayar untuk pekerjaan saya yang satu malam itu. 

Saya coba lagi di hotel lainnya di dekat Tour Eiffel \(sudah lupa namanya\). Ini juga hanya dua malam. Mungkin bahasa Perancis saya dianggap kurang baik, ketika menghadapi tamutamu. Makin lama saya makin panik, sebab persediaan uang terus berkurang, untuk makan, transport, dan sewa kamar yang sangat kecil yang terletak di rue Castagnary. Karena itu, berusaha terus dengan keras mencari juga “pekerjaan sementara,” dengan tiap hari melihat iklan-iklan di banyak gedung, termasuk di Alliance Française, tetapi tanpa hasil. 

Pada suatu hari saya menerima telegram dari Kantor Penempatan Tenaga di depan Gare de Lyon, supaya segera menghadap petugas kantor itu. \(Waktu itu, tidak punya telepon di kamar\). Segera saja keesokan harinya saya buru-buru menemui petugas itu \(wanita setengah tua yang amat simpatik\). Ia mengatakan bahwa ada permintaan tenaga, sebagai huissier \(upas kantor\) di salah satu badan di Kementerian Pertanian Perancis.

Bukan main senang hati saya waktu itu. Apalagi setelah tahu keesokan harinya, bahwa saya dapat diterima untuk bekerja di kantor Kementerian Pertanian itu. Beberapa bulan kemudian saya perlukan untuk menemuinya lagi \(namanya Mme David\), untuk menyatakan terima kasih bahwa berkat pertolongannya saya mendapat pekerjaan. Seandainya sekarang ini, sesudah dua puluh tujuh tahun kemudian, saya bisa bertemu dengan dia lagi, akan saya katakan kepadanya, bahwa pertolongan dia itu merupakan kejadian yang amat penting bagi “Perjalanan Hidup Saya.”


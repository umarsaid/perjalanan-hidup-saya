# Kegiatan-kegiatan mengenai Tapol dan Ex-tapol 

Ketika mulai tinggal di Paris sejak bulan September 1974, saya sudah menjadikan masalah Tapol dan peristiwa pembunuhan besar-besaran di Indonesia sebagai kegiatan utama. Sebab, masalah ini waktu itu merupakan isu yang bisa menarik perhatian banyak orang di Perancis. Waktu itu di pulau Buru sudah ditahan kira-kira sepuluh ribu orang, dan ratusan ribu tapol lainnya masih dipenjarakan dalam penjara-penjara yang tersebar di seluruh Indonesia. 

Dengan mengangkat masalah Tapol dan pembunuhan besarbesaran ini, maka secara langsung dan tidak langsung masalah politik pemerintahan Orde Baru bisa dipersoalkan di depan opini publik di Perancis. Dengan cara mengemukakan masalah yang kelihatannya hanya bersifat humaniter saja, sebenarnya dan pada akhirnya masalah politik juga terangkat. 

Ketika permintaan suaka belum diterima \(belum mendapat kartu dari OFPRA\), maka saya terpaksa hati-hati sekali dalam melakukan kegiatan ini. Karena, ketika menandatangani surat permintaan suaka, ada bagian di mana saya harus menyatakan bahwa saya tidak melakukan kegiatan politik. 

Tetapi, saya lakukan juga kegiatan-kegiatan ini, justru untuk menggalang relasi-relasi, yang dibutuhkan untuk mencari tumpuan berpijak yang lebih kuat.

Ketika sudah menerima kartu réfugié dari OFPRA pun, banyak kegiatan-kegiatan yang saya lakukan dengan cara-cara yang tidak terbuka. Oleh karena itu, di kalangan CCFD, Cimade, Amnesty International, Ligue des Droits de l’Homme atau organisasi-organisasi lainnya, saya dikenal oleh banyak teman dengan nama Alberto atau Nico. Untuk artikel yang disiarkan dalam Témoignage Chrétien saya gunakan nama Pierre Aubonne. 

Baru beberapa tahun kemudianlah saya menggunakan nama Umar Said dalam artikel yang dimuat oleh Le Monde Diplomatique dan majalah-majalah lainnya.

Kegiatan mengenai Tapol dan masalah-masalah situasi di Indonesia inilah yang telah membuka jalan bagi saya untuk mempunyai hubungan yang relatif cukup luas di Perancis.

Menurut pengamatan saya, mempersoalkan Tapol, Ex-tapol Indonesia dapat menimbulkan simpati banyak orang. Karena, orang melihat bahwa kita bekerja untuk kepentingan humaniter, untuk banyak orang yang sedang dianiaya dengan cara-cara yang kejam, dan untuk keluarga para Tapol dan Ex-tapol yang jumlahnya jutaan. 

Oleh karena itu, ketika dalam tahun 1977 saya minta kepada bekas Presiden CCFD \(Philippe Farine\) dan pimpinan Cimade \(Marcel Henriet\) untuk menjadi pengurus Komite Tapol Perancis, segera saja mereka menyatakan kesediaan mereka. 

Sejak itu, dengan berkantor di Rue Babylone nomor 68, saya bersama Hasibah \(wanita keturunan Aljazair\), mengadakan macam-macam kegiatan. Ini saya lakukan sambil bekerja terus di Kementerian Pertanian, dan dengan menggunakan nama samaran Alberto untuk hubungan-hubungan.

Dengan sengaja, nama saya tidak dicantumkan dalam susunan pengurus Komite Tapol ini, walaupun sebagian terbesar pekerjaan telah saya pikul. 

Di antara kegiatan-kegiatan yang agak menonjol dari Komite Tapol ini ialah petisi kepada pemerintah Indonesia untuk menuntut dipulihkannya secara penuh hak-hak dan kebebasan menulis bagi Pramoedya Ananta Toer dan rehabilitasi hak sipil bagi para Ex-Tapol. 

Petisi ini ditandatangani dalam tahun 1981 oleh tokoh-tokoh Partai Sosialis yang kemudian menduduki jabatan-jabatan penting dalam pemerintahan.

Tokoh-tokoh tersebut antara lain: Pierre Mauroy \(Perdana Menteri\), Alain Savary \(Menteri Pendidikan\), Piere Beregovoy \(Sekjen Kepresidenan dan kemudian Perdana Menteri juga\), Laurent Fabius \(kemudian menjabat Perdana Menteri\), Louis Mermaz \(Ketua Parlemen\), Pierre Joxe \(Menteri Dalam Negeri\), Jean Le Garrec, Gaston Deferre, Lionel Jospin, Paul Quiles, Jean Pierre Chevenement, Louis Le Pensec, Jean Pierre Cot, Marcel Debarge, Jean Poperen, dan lain-lain. 

Kegiatan mengenai soal Tapol, Ex-Tapol dan masalah demokrasi di Indonesia ini dilancarkan dengan bermacam-macam cara, mengeluarkan press-release, menerbitkan bulletin, membikin dossier, men-supply bahan-bahan, menghadiri rapat-rapat organisasi lainnya, ikut dalam Fête de l’Humanité. Pekerjaanpekerjaan ini telah dilakukan bertahun-tahun sejak 1975, pada umumnya sore atau malam hari sesudah keluar kantor SMAR dan pada hari-hari week-end. Penulisan artikel atau berbagai dossier telah saya lakukan sejak tinggal di kamar yang kecil dan sempit sekali \(2,5 x 3 m\) di daerah Bastille \(Rue de Lappe\).

Sudah tentu, berbagai kegiatan itu memakan waktu dan tenaga yang cukup banyak. Tetapi saya senang mengerjakannya, karena dengan begini bisa menjalin hubungan dengan banyak organisasi dan tokoh-tokoh berbagai kalangan. Hubungan-hubungan ini terbukti banyak gunanya di kemudian hari untuk menangani macam-macam urusan. 

Kemudian, dengan kedatangan teman-teman Indonesia lainnya, maka beranekaragam kegiatan-kegiatan  ini bisa dikerjakan bersama-sama. 

Pengalaman waktu itu menunjukkan bahwa apa yang terjadi di Indonesia di bawah Orde Baru menarik perhatian banyak orang. Banyak kasus yang mencerminkan adanya penindasan berskala besar dan luar biasa. Bagi banyak orang Perancis, atau orang Barat pada umumnya, beranekaragam tindakan pemerintah Indonesia waktu itu dianggap sebagai hal yang tidak bisa diterima oleh hati nurani dan akal sehat, dan karenanya perlu dikutuk. 

Surat-surat semacam ini kami terima dari berbagai pihak. Jerih payah mengadakan hubungan-hubungan ini akhirnya berbuah juga. Di samping ada orang-orang yang memang sensitif terhadap masalah-masalah perikemanusiaan ada juga organisasiorganisasi yang memang memikul misi untuk melawan kejahatan-kejahatan semacam masalah Tapol dan Ex-Tapol di Indonesia. Umpamanya: ACAT \(Action Chrétien pour l’Abolition de la Torture\), Commission Justice et Paix dari Dewan Uskup Perancis, Amnesty International, Cimade dll. 

Mereka ini, pada umumnya, senang untuk dihubungi dan diajak bekerja sama untuk melancarkan berbagai kegiatan. Dan banyak di antara mereka yang merasa bangga bisa berbuat sesuatu untuk orang-orang yang menjadi korban dari tindakan sewenang- wenang.

Tetapi, kegiatan semacam ini, untuk bisa berhasil juga memerlukan syarat-syarat, umpamanya: ketekunan, sikap yang korek, ketulusan hati dalam melaksanakan kegiatan, bisa menunjukkan integritas dan reputasi. Orang yang mempunyai reputasi yang baik akan lebih mudah untuk mendapatkan simpati dan kerjasama dari banyak pihak. 

Melakukan berbagai kegiatan mengenai soal-soal humaniter \(seperti Tapol dan Ex-Tapol\) dapat menjangkau lingkunganlingkungan yang luas, tanpa mempersoalkan secara tegas masalah ideologi. Sebab, di antara orang-orang atau organisasi-organisasi yang diajak kerjasama ini banyak juga yang anti-komunis. Dari pengalaman bisa dilihat bahwa banyak juga orang-orang Perancis \(atau dari bangsa lain\) yang tidak suka kepada ideologi komunis, tetapi mau membela orang-orang PKI atau yang dituduh PKI, hanya karena pertimbangan-pertimbangan perikemanusiaan, rasa keadilan dan akal sehat. 

Dengan dibebaskannya para Tapol dari pulau Buru dan puluhan ribu lainnya dari berbagai penjara, Komite Tapol makin berkurang kegiatannya, dan kemudian pengurus-pengurusnya \(yang resmi terbuka\) mengusulkan untuk dibubarkan saja, dan akhirnya dihentikan dalam tahun 1982. Pada waktu itu, saya sudah mengundurkan diri dari pekerjaan di Kementerian Pertanian Perancis, dan mulai menangani persiapan-persiapan untuk berdirinya SCOP Fraternité dan Restoran Indonesia. Pekerjaan ini telah memakan waktu dan tenaga, dan makin lama makin padat dalam beberapa tahun berikutnya.


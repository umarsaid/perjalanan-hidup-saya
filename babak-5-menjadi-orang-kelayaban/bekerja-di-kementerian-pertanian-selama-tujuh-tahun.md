# Bekerja di Kementerian Pertanian selama tujuh tahun 

Peristiwa diterimanya lamaran untuk bekerja di SMAR \(Société Mutualiste du Ministère d’Agriculture\) adalah bagian yang amat besar artinya bagi jalan hidup saya. Sebab, lewat masa inilah saya mulai bisa meletakkan dasar-dasar untuk kehidupan dan kegiatan-kegiatan selanjutnya di berbagai bidang di Perancis. Dan selama masa tujuh tahun bekerja di kantor itu pulalah terjadi berbagai peristiwa yang menjadi pengalaman penting bagi saya. 

Cerita tentang hari pertama kali saya menghadap pimpinan SMAR saja mengandung hal-hal yang tetap menjadi kenangkenangan sampai sekarang. Pada hari itu, pagi-pagi sekali saya mengatakan kepada huissier SMAR \(orang Perancis yang sudah tua sekali\) bahwa saya perlu menghadap pimpinan kantor. 

Seorang wanita muda \(Jocelyne Pagliarini\), sekretaris dari Sekjen SMAR, menerima saya. Saya jelaskan bahwa sudah menerima telegram dari Kantor Penempatan Tenaga yang menyatakan bahwa saya harus menghadap kantornya mengenai lowongan yang perlu diisi.

Jocelyne Pagliarini, waktu itu kelihatan keheran-heranan ada orang yang berwajah Asia yang melamar. Kemudian ia membawa saya untuk menghadap kepalanya, yaitu André Dussolier yang menjabat sebagai Sekjen. Di luar segala dugaan sebelumnya, Sekjen ini sangat ramah tamah.

Bahkan dalam wawancara ini ia menyebut saya sebagai “frère” \(saudara\). Dalam wawancara dengannya, saya jelaskan riwayat hidup dan mengapa datang ke Perancis untuk minta suaka politik. Karena penjelasan-penjelasan itu, maka diterimalah lamaran saya sebagai huissier \(upas kantor\) dengan percobaan selama tiga bulan.

Pekerjaan di kantor ini selama tujuh  tahun juga mengalami perobahan-perobahan. Mula-mula, tugas sehari-hari adalah pergi ke bank, mengamplopi surat-surat, dan pergi ke kantor pos. Kemudian, karena tulisan saya dianggap baik, diberi pekerjaan tulis menulis. Selanjutnya, ditugaskan untuk mengantar dan mengambil dokumen-dokumen dari berbagai departemen atau bagian dari Kementerian Pertanian. 

Saya berusaha bersikap baik terhadap semua pegawai. Di samping itu, berusaha terus memperbaiki bahasa Perancis saya. Sebab, saya pernah mengalami peristiwa yang cukup menyakitkan hati. Ketika ada seorang pegawai yang minta trombone \(jepitan kertas\) dan saya tidak mengerti apa arti trombone, maka pegawai ini mengatakan kepada teman-temannya bahwa saya “orang bodoh.” 

Sekjen SMAR, seorang Katolik yang juga menjadi Direktur majalah organisasi keagamaan yang bernama Pax Christi, bersikap baik sekali selama saya bekerja di kantor ini. Demikian juga sekretaris wanitanya, Jocelyne. Saya diperlakukan secara baik sekali, walaupun pekerjaan saya hanya sebagai huissier. Karena dalam tahun-tahun berikutnya ia tahu bahwa saya melakukan berbagai kegiatan mengenai Tapol di Indonesia, ia menghubungkan saya dengan salah seorang pendeta yang aktif di Pax Christi.

Masa-masa permulaan pekerjaan di SMAR merupakan periode adaptasi yang penting bagi saya dalam masyarakat Perancis. Di seluruh kantor yang pegawainya berjumlah sekitar tujuh puluh orang itu, hanya sayalah satu-satunya orang Asia. Dan ketika makan di ruangan kantin Kementerian yang besar itu, masih ada saja yang melihat saya dengan mata yang seperti keheran-heranan.

Perlakuan baik dari Sekjen itu nyata sekali ketika istri saya bisa datang untuk pertama kalinya ke Perancis. Dengan mudah dan simpati Sekjen kantor itu memberikan perlop kepada saya untuk menjemput istri di Belanda. Bahkan ia mengusahakan adanya bantuan \(sebesar 2000 F, yang waktu itu adalah jumlah yang lumayan\) untuk kedatangannya. Karena, selama bekerja beberapa tahun di kantor ini, ia mengetahui bahwa saya sudah lama berpisah dengan keluarga.

Kemudian, setelah istri sudah bisa datang ke Paris, dan sering saya bawa ke kantor \(suatu hal yang “luar biasa” juga\) dan menemani saya bekerja, ia tidak keberatan juga. Bahkan, sebagai hal yang lebih mengherankan lagi bagi banyak pegawai kantor itu, pernah beberapa kali saya dan istri diajak makan siang satu meja dengan pimpinan tertinggi kantor itu, yaitu Presiden SMAR. Ini juga merupakan satu sikap yang mempunyai arti besar bagi kami. 

Ketika mulai bekerja di SMAR, saya tinggal beberapa tahun di satu kamar kecil \(tanpa kamar mandi, dan WC-nya juga “umum,” artinya dipakai bersama-sama dengan penghuni gedung lainnya\) di rue de Lappe, depan Balajo \(Bastille\). Kemudian, dengan perantaraan SMAR, saya mendapat rumah HLM \(rumah sosial dengan sewa murah\) di Soisy sous Montmorency, yang terdiri dari dua kamar. Pada tahun 1980, juga dengan perantaraan SMAR, didapatkanlah rumah dengan empat kamar yang terletak di Noisy Le Grand.

Karena makin lama kegiatan-kegiatan politik dan sosial saya juga makin banyak, maka pekerjaan di SMAR itu juga sangat membantu. Saya sering menggunakan telepon kantor untuk halhal di luar urusan kantor. Kemudahan untuk minta cuti juga merupakan hal yang membantu berbagai kegiatan waktu itu. 

Ketika Sekjen André Dussolier sudah pensiun, saya masih beberapa kali bertemu. Ia menyatakan kegembiraannya mengetahui  kemajuan-kemajuan yang telah saya capai dalam berbagai hal. Ia pernah juga menghadiri hari ulang tahun Restoran Indonesia. Ketika saya sudah menerbitkan majalah ekonomi bulanan dalam bahasa Perancis Chine Express, kepadanya kadang-kadang juga saya kirimkan berbagai nomor. Ketika dalam tahun 1995 ia merayakan ulang tahun perkawinannya yang ke50, saya kirim ucapan selamat. Ini sebagai tanda terima kasih atas bantuannya yang diberikannya dalam tahun 1975 dan juga atas sikapnya yang bersahabat selama saya bekerja di badan Kementerian Pertanian Perancis itu.


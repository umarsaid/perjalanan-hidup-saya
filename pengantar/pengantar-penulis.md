# Pengantar Penulis

Dua hari berturut-turut, tanggal 19 dan 20 Mei 1995, di rumah seorang teman Indonesia di Paris, saya bertemu dengan Tjuk \(Alex\), teman sekelas ketika belajar di HIS Blitar \(sekolah Belanda\), lebih dari setengah abad yang lalu. Peristiwa ini bagi saya merupakan kejadian yang penting. Alasannya macam-macam. Karena, di samping perasaan gembira bertemu dengan teman semasa kecil, ketika berbincang-bincang tentang macam-macam soal yang terjadi di tanah air, kami juga berbicara tentang pengalaman-pengalaman kami berdua ketika ikut dalam pertempuran Surabaya di tahun 1945.

Kami saling mengingatkan bagaimana waktu itu, kami berdua bersama-sama belasan pelajar SMP Kediri dan SGL Blitar \(sebagian terbesar dari kelas terakhir\) telah meninggalkan kota Kediri dengan tekad untuk ikut bertempur di Surabaya. Percakapan kami tentang pertempuran Surabaya menyinggung macam-macam: markas BKR \(Badan Keamanan Rakyat\) di Darmo Boulevard 49, markas besar BPRI \(Barisan Pembrontak RI\) di hotel Simpang, pertempuran yang kami ikuti di Keputran dan Gunungsari dan lain-lain. Dan cerita tentang luka parah di kakinya karena ledakan mortir dan lain-lain.

Istri saya dan teman-teman Indonesia lainnya yang ikut menghadiri percakapan kami itu kelihatan terheran-heran mendengar cerita ini. Mereka baru tahu, bahwa dalam hidup saya ada juga bagian yang semacam ini. Memang, tidak banyak orang yang tahu tentang masalah ini dan berbagai masalah-masalah  
 lainnya yang telah saya alami. Termasuk istri saya sendiri. Dan juga anak-anak saya. Kejadian ini menyadarkan saya tentang perlunya membuat catatan atau mémoire.

Memang, sudah berkali-kali ada teman-teman, baik yang berasal dari Indonesia, maupun yang berasal dari negeri-negeri lain, yang mengusulkan supaya saya menulis sebuah mémoire atau membikin catatan tentang pengalaman hidup saya. Tahun 2004 ini, umur saya mencapai tujuh puluh enam tahun. Jadi, sudah tentulah banyak yang saya alami. Baik yang kecil-kecil dan tidak berarti atau yang bersifat biasa-biasa saja, maupun yang patut menjadi kenang-kenangan, bagi saya sendiri pribadi. Tetapi, barangkali juga untuk diketahui oleh keluarga besar saya dan orang-orang lainnya.

Saya berpisah dengan istri dan dua anak sejak September 1965. Pertemuan pertama kali dengan istri adalah sesudah tiga belas tahun putus hubungan \(tanpa kabar atau surat menyurat sama sekali\) dan dengan kedua anak laki-laki kami ketika mereka sudah berumur lebih dari delapan belas tahun.

Pertemuan kembali dengan mereka ini semuanya terjadi di Paris. Karena itu, banyak hal yang tak mereka ketahui tentang apa yang telah saya alami dan saya kerjakan di masa lampau.

Memang, selama ini saya tidak banyak bercerita kepada anakanak tentang masa lampau saya, dan istri saya pun tahu hanya sebagian atau sepotong-sepotong.

Saya juga sudah berpisah dengan adik-adik \(enam orang\) selama lebih dari  tiga puluh tahun. Mereka tinggal di berbagai daerah di Indonesia. Mereka, dan saudara-saudara lainnya, hanya mengira bahwa saya waktu itu ada di luar negeri. Bahkan mungkin, tadinya, ada yang menduga-duga bahwa saya sudah  
 mati atau hilang begitu saja. Oleh karena situasi di tanah air, dan karena berbagai sebab lainnya, sampai 1995 saya tidak punya hubungan dengan mereka.

Bagi mereka itu semuanya, catatan atau tulisan ini adalah sebagai “laporan,” atau pelengkap tentang apa yang sudah mereka ketahui selama ini.

Perjalanan Hidup Saya ini disusun secara pokok-pokok, dan bisa merupakan bahan dasar bagi penulisan-penulisan selanjutnya di kemudian hari. Karena, ada bagian-bagian yang memungkinkan penguraian lebih panjang. Misalnya, tentang sebagian dari pengalaman-pengalaman ketika menjadi wartawan di Indonesia, sebelum hidup lama di perantauan. Atau, kehidupan di Tiongkok selama tujuh tahun dapat merupakan penulisan tersendiri yang mungkin memerlukan banyak halaman. Demikian juga pengalaman yang padat di Perancis selama lebih dari tiga puluh tahun \(selama bekerja di Kementerian Pertanian Perancis, berdirinya restoran Indonesia di Paris, penerbitan majalah ekonomi bulanan Chine Express, Komite Tapol di Paris dan kegiatan-kegiatan lainnya\).

Karena situasi yang tidak mudah untuk memperoleh datadata \(tanggal-tanggal, peristiwa-peristiwa tertentu\) maka ada kemungkinan bahwa sejumlah data perlu disempurnakan.

Artinya, memoire ini memerlukan, secara berangsur-angsur, perbaikan atau penyempurnaan. Tulisan Perjalanan Hidup Saya ini ditulis di Paris, dan dimulai tanggal 25 Mei 1995.

Kemudian selama tujuh tahun tersimpan dan hanya menjadi bacaan sebagian kecil keluarga saya, atau hanya diketahui oleh sejumlah kecil kawan-kawan terdekat saya. Perlu ditegaskan di  
 sini, bahwa tulisan ini bukanlah ulasan yang bersifat sejarah mengenai masa-masa tertentu. Dan keterangan-keterangan mengenai masalah-masalah tertentu tidaklah punya pretensi sebagai pengkajian yang mendalam, tetapi hanya untuk menjelaskan hubungan saya dengan peristiwa-peristiwa atau keadaan.

Cerita-cerita mengenai masa kecil, masa muda, dan masa selama hidup saya di perantauan mudah-mudahan dapat membantu istri saya, anak-anak, para keluarga yang terdekat \(adik-adik dan lain-lain\) untuk lebih mengerti lagi tentang diri saya, tentang apa yang telah saya lakukan, dan tentang mengapa perjalanan hidup saya menjadi demikian.


# Catatan tentang tulisan ini

* Tulisan Perjalanan Hidup Saya ini mengutamakan persoalanpersoalan atau peristiwa-peristiwa yang ada hubungannya dengan persoalan saya atau diri-pribadi saya. Karena itu, dalam catatan-catatan ini banyak “saya”nya.
* Tulisan yang sekarang ini adalah tulisan yang dibuat dalam tahun 1995, ketika masih jaya-jayanya regime Orde Baru, yang semula dimaksudkan sebagai bahan bacaan bagi anggota keluarga saya saja, serta sahabat-sahabat yang terdekat.
* Ada hal-hal yang memang dengan sengaja tidak ditulis atau tidak dicantumkan dalam tulisan ini, disebabkan oleh pertimbangan-pertimbangan tertentu.
* Tetapi ada juga, tentunya, hal-hal yang seyogyanya ditulis tetapi tidak tertulis atau belum disajikan, yang disebabkan oleh ketidaksengajaan.
* Sejumlah nama teman-teman Indonesia yang dicantumkan dalam naskah ini dengan singkatan-singkatan, akan ditulis dengan nama lengkap dengan persetujuan mereka, dalam penyajian selanjutnya.




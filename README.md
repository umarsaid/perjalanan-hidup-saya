# Ringkasan

Buku _Perjalanan Hidup Saya_ ini berisi berbagai catatan tentang pengalaman seorang wartawan Indonesia, yang bisa dikategorikan “luar biasa”. Di dalamnya dapat dibaca sejarah hidup seorang wartawan yang amat berliku-liku,  penuh dengan gejolak dan peristiwa penting-penting. Pada umur tujuh belas tahun A. Umar Said sudah ikut dalam pertempuran 10 November di Surabaya yang kini terkenal sebagai Hari Pahlawan.  Pada umur dua puluh lima tahun ia ikut dalam Konferensi Internasional Hak-hak Pemuda di Wina \(Austria\) dan mengunjungi Tiongkok dalam tahun 1953 ketika Republik Rakyat Tiongkok baru empat tahun diproklamasikan. Kemudian selama tiga tahun menjadi wartawan _Harian Rakjat_, dan ketika pemberontakan PRRI pecah di tahun 1958, ia memimpin suratkabar _Harian Penerangan_ di Padang sambil melakukan gerakan di bawah-tanah.

Antara tahun 1960-1965 ia memimpin harian _Ekonomi Nasional_ di Jakarta, dan menjadi bendahara Persatuan Wartawan Asia-Afrika merangkap bendahara PWI-Pusat. Ia juga dipilih untuk menjadi bendahara Konferensi Internasional Anti Pangkalan Militer Asing \(KIAPMA\) di Jakarta tahun 1965 yang merupakan realisasi gagasan Bung Karno dalam perjuangan menentang imperialisme-kolonialisme waktu itu.

Ketika G30S meletus ia berada di Aljazair, dan kemudian terpaksa bermukim selama tujuh tahun di RRT sebelum minta suaka politik di Perancis, tempat ia hidup dan melakukan berbagai kegiatan sampai sekarang.

Buku ini dipilih sebagai Buku Bermutu oleh Program Pustaka - Yayasan Adikarya Ikapi melalui suatu proses seleksi penilaian kompetitif dan selektif. Program Pustaka merupakan program bantuan penerbitan buku-buku bermutu, hasil kerja sama antara Yayasan Adikarya Ikapi dan The Ford Foundation, tetapi The Ford Foundation tidak terlibat dalam proses seleksi naskah.

CATATAN: Anda dapat mengakses file mentah buku ini dalam buku [GitLab Repository](https://gitlab.com/umarsaid/perjalanan-hidup-saya).


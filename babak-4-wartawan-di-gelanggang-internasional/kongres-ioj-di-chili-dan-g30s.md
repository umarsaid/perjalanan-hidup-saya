# Kongres I.O.J. di Chili dan G30S

Dalam bulan September 1965 di Santiago \(ibukota Chili\)dilangsungkan Konferensi I.O.J. \(Organisasi Wartawan Internasional\). Sudah sejak beberapa tahun sebelumnya, sejumlah wartawan Indonesia menggabungkan diri dalam Grup I.O.J. Indonesia.

Untuk konferensi di Chili ini grup Indonesia ini juga mendapat undangan untuk hadir dalam kongres itu, sebab Indonesia menduduki tempat sebagai salah satu dari beberapa Wakil Presiden organisasi internasional ini.

Grup IOJ Indonesia telah menunjuk saya untuk ikut dalam delegasi Indonesia untuk menghadiri kongres ini. Delegasi Indonesia itu terdiri dari tiga orang, yaitu Francisca Fangiday yang mewakili Harian Rakjat, seorang dari suratkabar Suluh Indonesia dan saya. Kecuali itu, Sekretariat PWAA juga menugaskan saya untuk singgah di Aljazair, dalam perjalanan kembali dari Santiago menuju tanah air. Tugas saya waktu itu ialah untuk membicarakan dengan persatuan wartawan Aljazair mengenai persiapan KWAA yang ke-2. Karena, dalam sidangsidang sebelumnya, konferensi PWAA telah memutuskan untuk menyelenggarakan KWAA yang ke-2 di benua Afrika, dan Aljazairlah yang diputuskan untuk menyelenggarakannya. 

Pada tanggal 14 malam kami bertiga berangkatlah dari Kemayoran dengan pesawat CSA \(Cekoslowakia\) menuju Praha dan kemudian ke Santiago. Keberangkatan saya kali ini diantar oleh istri dengan becak ke lapangan terbang Kemayoran, yang letaknya tidak jauh dari rumah kami di Kepu Selatan. Rupanya, sejak kali inilah kami berpisah lama sekali, dan baru tiga belas tahun kemudian bertemu kembali di lapangan terbang Schiphol \(Amsterdam\).

Dalam sidang-sidang IOJ di Santiago ini delegasi kami menghadapi sikap yang tidak begitu hangat lagi dari delegasi Soviet beserta pendukung-pendukungnya, yang terdiri dari negerinegeri Eropa Timur lainnya. Kami bekerja sama erat dengan delegasi-delegasi persatuan wartawan RRT \(dipimpin oleh Li Pingchuan\), dari Jepang, Korea Utara, dan Vietnam. Ketika itu, pertentangan ideologi antara Peking dan Moskow makin menajam, dan ini tercermin juga dalam sidang-sidang. 

Walaupun begitu, hubungan pribadi saya dengan Presiden IOJ \(Maurice Hermann, dari Perancis\) dan Sekjennya \(Jiri Meisner, dari Cekoslowakia\) tetap baik.

Setelah konferensi IOJ selesai, ada program yang diatur oleh Panitia setempat untuk mengunjungi berbagai tempat di Chili. Francisca Fangiday dan teman dari Suluh Indonesia mengikuti program itu, antara lain dengan mengunjungi kota Valparaiso dan daerah pertambangan. Tetapi, karena ada tugas ke Aljazair, maka saya meninggalkan Chili lebih dahulu. Saya menuju Paris dan kemudian ke Alger. Ini terjadi akhir bulan September 1965. 

Sesudah tinggal beberapa hari di Alger, maka pada tanggal 1 Oktober, tersiar berita terjadinya peristiwa G-30-S di Jakarta. Selama beberapa hari saya sering menghubungi KBRI untuk menanyakan kelanjutan perkembangan peristiwa ini. Selain itu, dalam hari-hari itu, selalu mempertimbangkan tindakan apakah yang harus saya lakukan selanjutnya: kembali pulang ke Jakartakah atau bagaimana? 

Kemudian tersiarlah berita bahwa sejumlah suratkabar ditutup, termasuk Ekonomi Nasional dan Wartabhakti. Sejak itu, saya putuskan untuk tidak kembali dulu ke Jakarta, sambil menunggu perkembangan selanjutnya. Waktu itu saya tahu bahwa di Peking sedang ada delegasi Indonesia yang besar, \(yang terdiri dari berbagai organisasi, termasuk juga delegasi resmi pemerintah yang diketuai oleh Wakil PM Chaerul Saleh\) untuk menghadiri perayaan Hari Nasional Tiongkok 1 Oktober. Sebelum meninggalkan Jakarta menuju Chili, saya sudah mendengar bahwa sejumlah wartawan ikut dalam delegasi PWI dalam perayaan 1 Oktober itu \(antara lain Soepeno, Ketua PWI Pusat\).

Dari Alger saya mengadakan kontak dengan teman-teman delegasi PWI yang ada di Peking. Kemudian saya mendapat anjuran dari Soepeno untuk tidak kembali langsung ke Jakarta, tetapi supaya singgah dulu ke Peking untuk mengikuti perkembangan selanjutnya. Di Paris saya tinggal beberapa hari, dan akhirnya bertemu dengan Francisca Fangiday di kota ini, yang meninggalkan Chili setelah selesai dengan acara kunjungannya ke Valparaiso dan daerah lainnya. 

Dalam perundingan dengan Francisca di Paris itu kami mengambil kesimpulan bahwa sebaiknya kami berdua menggabungkan diri dengan delegasi PWI yang sedang berada di Peking waktu itu, dan tidak kembali langsung ke Jakarta dulu. Sebab, makin banyak berita yang tersiar bahwa sejumlah besar kawan-kawan telah ditangkapi dan dibunuhi, dan situasi di Indonesia makin tidak menentu.

Maka berangkatlah saya dari Paris, pada pertengahan atau akhir Oktober 1965, menuju Peking. Di Peking saya bertemu dengan banyak orang Indonesia, yang menjadi tamu pemerintah Tiongkok dalam rangka perayaan Hari Nasional 1 Oktober itu. Mereka terdiri dari orang-orang yang tergabung dalam delegasi PKI, SOBSI, Lekra, BTI, Pemuda Rakyat, HSI, Gerwani, dan organisasi-organisasi massa lainnya. Di samping itu, ada juga delegasi resmi pemerintah Indonesia atau delegasi MPRS.

Dengan kedatangan di Peking dalam akhir tahun 1965 maka mulailah, sejak itu, kehidupan saya di Tiongkok selama tujuh tahun.

